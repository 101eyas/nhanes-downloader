from requests_html import HTMLSession
from urllib.parse import urlparse
import pandas

### Constants ### 
# Selector for getting box of links to datasets 
DDC_CSS_selector = "#form1 > div.bg-white.body-wrapper.container.d-flex.flex-wrap > main > div:nth-child(4) > div > div:nth-child(3) > div > div > div > div:nth-child(1) > div:nth-child(1) > div.card-body.bg-white.no-padding > ul"

def parse(url):
    # What should this do? 
    # - Get page, get all subsections in the correct box (Data, Documentation, Codebooks section)
    #   - Each subsection (demographics, dietary, examination, laboratory, questionnaire, etc. Ignore limited.) has 
    #       similar structure and can be processed the same way. 
    #   - So, for each subsection, do the following: 
    #       - Create "parent" structure (folder, whatever) for DDC 
    #       - Each subsection has the following in a list: 
    #           - Doc file object 
    #           - Data file object (parsed to JSON)
    # Example result object: 
#   example = {
#           2015: [
#               "Demographics": {
#
#                    },
#                "Dietary": {
#
#                    },
#                "Examination": {
#
#                    },
#                "Laboratory": {
#
#                    },
#                "Questionnaire": {
#                    
#                    }
#                ]
#            }
    # So, actual code steps:
    # Get datapages 
    # Parse each datapage 
    # 

    # Old-new
    # - Entry point into parser logic 
    # - Return list of objects: 
    #   - Objects returned:
    #       - Pairs of codebooks and JSON data 
    # OLD SHIT 
    # table = pandas.read_html(url)
    # For each row in table need to get title, get file, get codebook shit, put file and codebook shit together, then return as unified object etc. 
    # print(table[0])
    # xpt_remote_file_test = pandas.read_sas("https://wwwn.cdc.gov/Nchs/Nhanes/2015-2016/DEMO_I.XPT")
    datapage_links = get_datapages(url, DDC_CSS_selector)
    return datapage_links

def get_datapages(link, container_selector):
    sn = HTMLSession()
    request = sn.get(link)
    # TODO: Get label for each link from the "?Component=<label>" part of URL. Pass back dictionary instead of list. 
    links = request.html.find(container_selector, first=True).links
    return links

def parse_codebook(link): 
    pass

def parse_dataset(link):
    pass
