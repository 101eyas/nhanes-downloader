import argparse
from data_year_parser import parse 
from datetime import date
import pprint

# constants 
continuous_NHANES_root_page = "https://wwwn.cdc.gov/nchs/nhanes/continuousnhanes/default.aspx"
continuous_NHANES_year_exceptions = {
            2019: "Cycle=2017-2020",
            2020: "Cycle=2017-2020",
        }

# function to parse links from file
def parse_links_file(filepath):
    lines = []
    with open(filepath, "r") as f:
        lines = f.readlines()
    if(len(lines) == 0):
        raise ValueError("cannot import data without at least one URL. Omit the file input if you want to download all data.")    
    lines = list(map(lambda s : s.strip(),  lines))
    # TODO: Check to see if it is a valid url and discard if not. 
    return lines

def is_valid_connection_string():
    # TODO: Actual implementation to check if this is a valid connection string
    return False

def get_args():
    parser = argparse.ArgumentParser()
    connection_help = "Supply connection string to PostgreSQL database to load as tables and views. "
    connection_help += "Must have permission to create database, schema, tables and views."
    parser.add_argument("-c", "--connection", help=connection_help)
    range_help = "Provide a range of years for determining which years of data to download. "
    range_help += "Use the following format: 'Y1Y1-Y2Y2' where Y1Y1 is the starting year of the range and Y2Y2 is the end year of the range.\n"
    range_help += "Please keep in mind that data for 2019-2020 was combined with 2017-2018 due to Covid. "
    range_help += "Including 2019 or 2020 in your range will download the augmented dataset."
    parser.add_argument("-r", "--range", help=range_help)
    args = parser.parse_args()
    return args

def parse_range(range_string, current_year):
    # TODO: Use regex to validate input
    endpoints = range_string.split("-")
    start = int(endpoints[0])
    end = int(endpoints[1])
    comparison_year = current_year - 2
    if end > comparison_year:
        print(f"WARNING: End year \"{end}\" is invalid. Truncating to {comparison_year}.")
        end = comparison_year
    return [year for year in range(start, end)]

def get_all_years(root, exceptions, years):
    links = {} # Use a set so that duplicate entries are automatically ignored
    for year in years:
        link_suffix = "?"
        if year in exceptions:
            # use exception value 
            link_suffix += exceptions[year]
        else: 
            # use normal year value 
            link_suffix += f"BeginYear={year}"
        links[year] = root + link_suffix   
    return links

def main():
    args = get_args()
       # Download requested data
    links = get_all_years(continuous_NHANES_root_page, continuous_NHANES_year_exceptions, parse_range(args.range, date.today().year))
    data = {}
    for year in links.keys():
        # For each year process the link
        data[year] = parse(links[year])

    # TEST SHIT
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(data)
    # TEST SHIT END 


    # TODO: Parse data according to options 
    # TODO: Write data according to options 

    # New architecture: 
    # - Process flags and store in local vars
    # - Probably something like get all years, then all categories/whatever for each year, get all datasets 
    #   - "Data, Documentation, Codebooks" section consistent from year to year 
    #       - Demographics
    #       - Dietary
    #       - Examination 
    #       - Laboratory 
    #       - Questionnaire 
    #       - Limited Access (no reason to do anything)
    #   - Should be able to do the following for every subsection ("DDC"):
    #       - iterate through table and get each row
    #       - For each row, get the doc file and the data file
    #       - For the doc file parse through the HTML and extract the following per column header in the "codebook":
    #           - Code/Value information: 
    #              - Whether range of values or discrete 
    #           - Retain mapping for each column for later 
    #       - For the data file, download xpt and parse to some kind of internal data structure
    #       - Once doc file and data file processed and in memory, then we can map them together? 
    #           - Not really any reason to, would just duplicate header data in memory before we need it
    # - At this point path diverges between write to DB and write to file. This should only be fleshed out once everything else done
    #   - "Everything else" being downloading and parsing all data and header data 
    #       - DB:
    #           - Create DB for entire dataset, flag to just write to existing database. Intended usage is to not do that.
    #           - Create table per DDC 
    #       - Excel:
    #           - For each "DDC" create folder, 


if __name__ == "__main__":
    main()
