# Introduction 
This project was originally a subcomponent of a personal data analysis project that ended up not being realistic: Data I needed for what I was interested in was not publicly available. However, I noticed that most NHANES download scripts had various problems (many of them are quite old and at least one is broken, some just download the XPT files without parsing to something you can use without a SAS application) and I never found one that could write data to a relational database, which is what I was interested in, so I decided to finish the download application by itself and release it. 

# Project Status
WIP 

# Usage 
TBD 

# Additional download applications
In case this project does not satisfy your needs, here are links to some of the others I found before starting work on this one. I am completely unaffiliated with the authors of these projects and can make no promises on whatever state their code is in, whether it works, how secure or insecure it is, etc. 
- https://github.com/mirador/nhanes
- https://github.com/LeviButcher/nhanse-dl
- https://github.com/mrwyattii/NHANES-Downloader

# TODO
- Finish basic functionality (only real goal at this point)  
- Async/multiprocess webscraping 
- More validation on input
